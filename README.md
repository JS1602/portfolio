# Portfolio

### Hi 👋 I'm Jörg! People also call me George.
<br>

## 🚀 About Me
<p style="text-align: center; padding: 15px; font-style:italic">
I am a passionate fullstack developer in his 20's. <br> 
I am currently partaking in an apprenticeship at 
<a href="https://codersbay.wien/"><b>@CODERS.BAY - Vienna</b></a>.
as an app developer, which will end in June 2024. <br> <br>
Throughout this time, I gained knowledge and skills in frontend and backend development, databases, and agile project management with Scrum. <br><br>
I am a highly motivated and creative person who thrives on teamwork, and I am thrilled to use my kit to contribute to
the development of innovative applications.
✌️
</p>

* 🌍 I'm based in Vienna
* 📫 ️ How to reach me: **[joerg.fasp@gmail.com](mailto:joerg.fasp@gmail.com)**
* 🌱 I’m currently learning **Jetpack Compose / Kotlin**
* 🔭 I’m currently working on **CaRent**
## 🔗 Links
[![linkedin](https://img.shields.io/badge/linkedin-0A66C2?style=for-the-badge&logo=linkedin&logoColor=white)](https://www.linkedin.com/in/j%C3%B6rg-spitzer-94a9a4245/)
[![GitHub](https://img.shields.io/badge/github-%23121011.svg?style=for-the-badge&logo=github&logoColor=white)](https://www.github.com/JSP3)

<br>
<br>

# 🛠 My Kit


### Web
<p align="left">
<a href="https://www.w3.org/html/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="50" height="50"/> </a> 
<a href="https://www.w3schools.com/css/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="50" height="50"/> </a>
<a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="50" height="50"/> </a>
<a href="https://getbootstrap.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="50" height="50"/> </a>
<a href="https://vuejs.org/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg" alt="vuejs" width="50" height="50"/> </a> 
<a href="https://vuetifyjs.com/en/" target="_blank" rel="noreferrer"> <img src="https://bestofjs.org/logos/vuetify.svg" alt="vuetify" width="50" height="50"/> </a>
<a href="https://nodejs.org/en/" target="_blank" rel="noreferrer"><img src="https://raw.githubusercontent.com/danielcranney/readme-generator/main/public/icons/skills/nodejs-colored.svg" width="50" height="50" alt="NodeJS" /></a>
</p>
<br>

### Backend
<p align="left">
<a href="https://www.java.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="50" height="50"/> </a> 
<a href="https://spring.io/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="50" height="50"/> </a>
<a href="https://www.php.net/"><img src="https://www.vectorlogo.zone/logos/php/php-ar21.svg" alt="php" width="75" height="50"/></a>
<a href="" target="_blank" rel="noreferrer"><img src="https://www.vectorlogo.zone/logos/laravel/laravel-ar21.svg" alt="laravel" width="110" height="50"/></a>
</p>
<br>

### Mobile
<p align="left">
<a href="https://kotlinlang.org" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-icon.svg" alt="kotlin" width="50" height="50"/> </a>
<a href="https://developer.android.com" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original-wordmark.svg" alt="android" width="50" height="50"/> </a>
</p>
<br>

### Database
<p align="left">
<a href="https://www.mysql.com/" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/mysql/mysql-original-wordmark.svg" alt="mysql" width="50" height="50"/> </a>
<a href="https://www.postgresql.org" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/postgresql/postgresql-original-wordmark.svg" alt="postgresql" width="50" height="50"/> </a> 
<a href="https://www.h2database.com" target="_blank" rel="noreferrer"> <img src="https://dbdb.io/media/logos/h2-logo.svg" alt="h2" width="50" height="50"/></a>
</p>
<br>

### Design & Prototyping
<p align="left">
<a href="https://www.adobe.com/at/products/aftereffects.html" target="_blank" rel="noreferrer"> <img src="https://www.adobe.com/content/dam/cc/us/en/products/ccoverview/ae_cc_app_RGB.svg" alt="Adobe After Effects" width="50" height="50"></a>
<a href="https://www.photoshop.com/en" target="_blank" rel="noreferrer"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="50" height="50"/> </a>
<a href="https://www.figma.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="50" height="50"/></a>
<a href="https://www.lottiefiles.com"> <img src="https://static10.lottiefiles.com/images/logo/icon.svg" alt="Lottie-files" width="50" height="50"></a>
</p>
<br>

### Tools
<p align="left">
<a href="https://postman.com" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="50" height="50"/> </a>
</p>
<br>

### Version Control
<p align="left">
<a href="https://git-scm.com/" target="_blank" rel="noreferrer"> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="50" height="50"/> </a>
</p>
<br><br>

# 👨‍💻 Projects

### [ZyBer ](https://gitlab.com/JS1602/vuestrap_semesterproject) - Fullstack Spring Boot and Vue.js Project

Within the first semester at CODERS.BAY Vienna, I worked with my college Oliver Alcantara to build a website.
It is a fullstack administration application to manage students and teachers which helped us get to know the concepts of CRUD (Create, Read, Update, Delete) operations and how frontend and backend requests work together to deliver the desired functionality.
I worked on both front & back - end, with more focus on the frontend aspect of the application.

### Stack
- #### Backend: 
  - <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/java/java-original.svg" alt="java" width="35" height="35"/> <img src="https://www.vectorlogo.zone/logos/springio/springio-icon.svg" alt="spring" width="35" height="35"/> <img src="https://dbdb.io/media/logos/h2-logo.svg" alt="h2" width="35" height="35"/>

- #### Frontend:
  - <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="35" height="35"/> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="35" height="35"/><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="35" height="35"/><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="35" height="35"/><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/vuejs/vuejs-original-wordmark.svg" alt="vuejs" width="35" height="35"/><img src="https://bestofjs.org/logos/vuetify.svg" alt="vuetify" width="35" height="35"/>

- #### Tools:
  - <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="35" height="35"/> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="35" height="35"/> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="35" height="35"/>

<br>
<br>

### [JDM](https://gitlab.com/JS1602/jdm) - First Frontend Projekt

The very first solo Project we had in the first semester at CODERS.BAY Vienna was to build a website with <b>HTML, CSS</b> and <b>JavaScript </b>.
We were given free rein to choose the topic and design of the website. Since I am a big fan of the Automotive Industrie, 
I decided to build a website about some legends of the car industry.

### Stack
- #### Frontend:
  - <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/html5/html5-original-wordmark.svg" alt="html5" width="35" height="35"/> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/css3/css3-original-wordmark.svg" alt="css3" width="35" height="35"/><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/javascript/javascript-original.svg" alt="javascript" width="35" height="35"/><img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/bootstrap/bootstrap-plain-wordmark.svg" alt="bootstrap" width="35" height="35"/>

- #### Tools:
  - <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="35" height="35"/> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="35" height="35"/>

<br>
<br>


### [CaRent](https://gitlab.com/JS1602/carent) - Kotlin Jetpack Compose Project

The Android Application (MVVM) in our second semester I worked on at Coders.Bay.
CaRent is an application which lets the user share their vehicles while on vacation or whatever reason they might have.
It gave me an understanding of a lot of things. <b> Kotlin, Jetpack Compose, libraries like Coil, Room, Navigation, Lottie </b> as well as 
the concept of handling <b>states</b> and <b>recompositions</b>. 
The use of <b>After Effects & Photoshop</b> boosted my curiosity even more and helped me reach new insights in UI & UX design.

### Stack
- #### Backend:
  - <img src="https://www.vectorlogo.zone/logos/kotlinlang/kotlinlang-icon.svg" alt="kotlin" width="35" height="35"/> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/android/android-original-wordmark.svg" alt="android" width="35" height="35"/> <img src="https://coil-kt.github.io/coil/logo.svg" alt="coil" width="75" height="35"/> 

- #### Design:
  -  <img src="https://www.adobe.com/content/dam/cc/us/en/products/ccoverview/ae_cc_app_RGB.svg" alt="Adobe After Effects" width="35" height="35"> <img src="https://raw.githubusercontent.com/devicons/devicon/master/icons/photoshop/photoshop-line.svg" alt="photoshop" width="35" height="35"/> <img src="https://www.vectorlogo.zone/logos/figma/figma-icon.svg" alt="figma" width="35" height="35"/> <img src="https://static10.lottiefiles.com/images/logo/icon.svg" alt="Lottie-files" width="35" height="35">
  
- #### Tools:
  - <img src="https://www.vectorlogo.zone/logos/getpostman/getpostman-icon.svg" alt="postman" width="35" height="35"/> <img src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt="git" width="35" height="35"/>

<br>
<br>

### [PlasticSlug](https://gitlab.com/codersbay-fia-3/hello-world-2d-game) - 2D Java Game

TBA

### Stack
- #### Backend:

- #### Design:

- #### Tools:

